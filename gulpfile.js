const { src, dest, series, parallel } = require('gulp');
const autoprefixer = require('autoprefixer');
const postcss = require('gulp-postcss');
const sass = require('gulp-dart-sass');
const hbs = require('gulp-compile-handlebars');
const inline = require('gulp-inline-css');
const rename = require('gulp-rename');

// -- Configs ----------------------------------------

const postCssPlugins = [
	autoprefixer()
];

const handlebarsSettings = {
	ignorePartials: true, // Allow using the {#> ...}  notation
	batch: [ './src/partials' ], // Allow handlebars to find imports
}

// -- Tasks ------------------------------------------

function compileSass() {
	return src('src/sass/styles.scss')
		.pipe(sass())
		.pipe(postcss(postCssPlugins))
		.pipe(rename('styles.css'))
		.pipe(dest('build/'));
}

function compileHandlebars() {
	return src('src/index.hbs')
		.pipe(hbs(null, handlebarsSettings))
		.pipe(rename('index.html'))
		.pipe(dest('build/'));
}

function inlineCss() {
	return src('build/index.html')
		.pipe(inline())
		.pipe(dest('dist'));
}

// ---------------------------------------------------

exports.default = series(parallel(compileSass, compileHandlebars), inlineCss);
exports.compileSass = compileSass;
exports.compileHandlebars = compileHandlebars;